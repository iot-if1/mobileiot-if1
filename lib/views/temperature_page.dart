import 'dart:io';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:webview_flutter/webview_flutter.dart';

class TemperaturePage extends StatelessWidget {
  const TemperaturePage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(),
      body: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          const Text(
            'Monitor Suhu',
            style: TextStyle(fontSize: 20),
          ),
          const SizedBox(height: 20),
          if (!kIsWeb &&
              Platform.isAndroid) // Check if not running on web and is Android
            SizedBox(
              width: double.infinity,
              height: 260,
              child: Container(
                  child: WebView(
                initialUrl: Uri.dataFromString(
                        '<html><body><iframe width="100%" height="260" style="border: 1px solid #cccccc;" src="https://thingspeak.com/channels/2324439/charts/1?bgcolor=%23ffffff&color=%23d62020&dynamic=true&results=60&type=line"></iframe></body></html>',
                        mimeType: 'text/html')
                    .toString(),
                javascriptMode: JavascriptMode.unrestricted,
              )),
            ),
          if (kIsWeb || Platform.isIOS) // Check if running on web or is iOS
            Container(
              child: const Text(
                'WebView is not supported on this platform.',
                style: TextStyle(fontSize: 16),
              ),
            ),
        ],
      ),
    );
  }
}

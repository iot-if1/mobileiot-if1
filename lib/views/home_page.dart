import 'package:flutter/material.dart';
import 'package:mobileiot_if1/views/humidity_page.dart';
import 'package:mobileiot_if1/views/temperature_page.dart';

class HomePage extends StatelessWidget {
  const HomePage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      home: Scaffold(
        appBar: AppBar(
          title: const Text("IOT IF-1"),
        ),
        body: Center(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              Text(
                'M. Fikri Aqsha Zulfa Ismail'.toUpperCase(),
                style: const TextStyle(
                  fontSize: 20,
                  fontWeight: FontWeight.bold,
                ),
              ),
              const SizedBox(height: 20),
              SizedBox(
                width: 150,
                child: ElevatedButton(
                  onPressed: () {
                    Navigator.push(
                      context,
                      MaterialPageRoute(
                        builder: (context) => const TemperaturePage(),
                      ),
                    );
                  },
                  child: Text('Suhu'.toUpperCase()),
                ),
              ),
              const SizedBox(height: 10),
              SizedBox(
                width: 150,
                child: ElevatedButton(
                  onPressed: () {
                    Navigator.push(
                      context,
                      MaterialPageRoute(
                        builder: (context) => const HumidityPage(),
                      ),
                    );
                  },
                  child: Text('Kelembapan'.toUpperCase()),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}

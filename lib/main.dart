import 'package:flutter/material.dart';
import 'package:mobileiot_if1/views/home_page.dart';

void main() {
  runApp(const IotApp());
}

class IotApp extends StatelessWidget {
  const IotApp({super.key});

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      theme: ThemeData(
        colorScheme: ColorScheme.fromSeed(seedColor: Colors.deepPurple),
        useMaterial3: true,
      ),
      home: const HomePage(),
    );
  }
}
